.. Sphinx Example Project documentation master file.

Welcome to Sphinx Example Project's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2


UML Example
===========

.. uml::

   Alice -> Bob: Hi!
   Alice <- Bob: How are you?


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

